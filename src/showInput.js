import React, { useState, useRef } from 'react';
import { View, StyleSheet, Button, Text } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

const showInput = () => {
    const [text, setText] = useState('');
    let textF = useRef(null);

    handleClickButton = () => {
        setText(textF.current),
            console.log('')
    }

    handleChangTextC = (text) => {
        textF.current = text
    }
    return (
        <View style={styles.container}>
            <Text style={styles.text} editable={false} >
                {text}
            </Text>
            <TextInput style={styles.input}
                onChangeText={handleChangTextC}
            />
            <TouchableOpacity onPress={handleClickButton}>
                <View style={styles.button} >
                    <Text style={styles.btntext}>Show</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 10,
        padding: 10,
        backgroundColor: "red",
    },
    text: {
        fontSize: 14,
        width: '100%',
        color: '#fff',
        minHeight: 40,
    },
    input: {
        backgroundColor: '#fff',
        marginRight: 20,
        borderRadius: 5,
        paddingHorizontal: 10,
        textAlignVertical: "top",
        color: '#000',
        height: 50
    },
    button: {
        alignItems: 'center',
        marginVertical: 10
    },
    btntext: {
        backgroundColor: '#eee',
        padding: 20,
        borderRadius: 5
    }
});

export default showInput;