import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TextInput, Image, Text } from 'react-native'

const Item = () => {

    return (
        <View style={styles.item}>
            <View style={styles.itemCT}>
                <Image style={styles.img} source={require('../public/img/image_1.png')}></Image>
            </View>
            <Text style={styles.text} numberOfLines={2}>
                123
            </Text>
        </View>
    )
}

const layout1 = () => {
    const [value, onChangeText] = React.useState('add key');

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.header}>
                <TextInput style={styles.hdinput}
                    onChangeText={text => onChangeText(text)}
                    value={value}
                />
            </View>

            <View style={styles.body}>
                <Item />
                <Item />
                <Item />
                <Item />
            </View>

            <View style={styles.basic}>
                <View style={styles.key}>
                    <Image source={require('../public/img/image_8.png')} />
                </View>
                <View style={styles.label}>
                    <Text style={[styles.bold, styles.labelT]}>Basic 2</Text>
                    <Text style={styles.labelT}>Start your deepen you practice</Text>
                </View>
            </View>
            <View style={styles.footer}>
                <View style={styles.ftImg}>
                    <Image style={styles.Img} source={require("../public/img/image_5.png")} />
                    <Text style={styles.ftText}>Today</Text>
                </View>
                <View style={styles.ftImg}>
                    <Image style={styles.Img} source={require("../public/img/image_6.png")} />
                    <Text style={styles.ftText}>All Exercises</Text>
                </View>
                <View style={styles.ftImg}>
                    <Image style={styles.Img} source={require("../public/img/image_7.png")} />
                    <Text style={styles.ftText}>Settings</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'pink',
        alignItems: "center"
    },
    hdinput: {
        backgroundColor: "#fff",
        height: 40,
        width: 300,
        marginTop: 15,
        marginBottom: 15,
        borderRadius: 20,
        paddingHorizontal: 20
    },
    body: {
        flex: 1,
        marginVertical: 20,
        marginHorizontal: 15,
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "stretch"
    },
    item: {
        height: '50%',
        backgroundColor: "#fff",
        borderRadius: 10,
        alignItems: "center",
        borderWidth: 1,
        borderColor: "#fff",
        marginBottom: 20,
        marginHorizontal: 10,
        paddingVertical: 20,
        width: '44%'
    },
    itemCT: {
        flex: 1,
        minHeight: 110,
        justifyContent: "center"
    },
    img: {
        resizeMode: "contain",
        width: 120,
        height: 120
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 20,
        justifyContent: "center",
    },
    basic: {
        flexDirection: "row",
        backgroundColor: "#fff",
        marginHorizontal: 21,
        marginVertical: 15,
        borderRadius: 10,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20
    },
    key: {
        flex: 1,
        textAlign: "center",
    },
    label: {
        flex: 4,
    },
    labelT: {
        fontSize: 16
    },
    bold: {
        fontWeight: "bold"
    },
    footer: {
        flexDirection: "row",
        height: 60,
        backgroundColor: "#fff",
    },
    ftImg: {
        flex: 1,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    ftText: {
        fontSize: 16,
        marginTop: 5,
    }
})

export default layout1;