import * as React from 'react';
import { View, Text, Button, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function HomeScreen() {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
        </View>
    );
}

function LogoTitle() {
    return (
        <Image
            style={{ width: 50, height: 50 }}
            source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}
        />
    );
}

const HeaderButton = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        headerLeft: () => (
                            <Button
                                onPress={() => console.log('left') }
                                title="Info"
                                color="#00cc00"
                            />
                        ),
                        headerRight: () => (
                            <Button
                                onPress={() => console.log('right') }
                                title="Info"
                                color="#00cc00"
                            />
                        ),
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default HeaderButton;
