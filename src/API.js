import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, FlatList, TextInput } from 'react-native';

const API = (props) => {
    const [text, setText] = useState('');
    const [dataArr, setDataArr] = useState([]);
    const [dataDis, setDataDis] = useState([]);
    // render API
    const fetchAPI = () => {
        return fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(data => {
                setDataArr(data);
                setDataDis(data);
                // console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    }
    useEffect(() => {
        fetchAPI();
    }, []);

    // border bottom
    const itemSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: '100%',
                    backgroundColor: '#000',
                }}
            />
        )
    }

    // search key
    const searchKey = (text) => {
        if (text === 0) {
            setDataDis(dataArr)
        } else {
            let result = dataArr.filter(item => item.title.toUpperCase().indexOf(text.toUpperCase()) !== -1);
            setDataDis(result);
        }
    }
    useEffect(() => {
        searchKey(text);
    }, [text]);

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.MainContainer}>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => setText(text)}
                    value={text}
                    placeholder="Search Key"
                />
                <FlatList
                    data={dataDis}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={itemSeparator}
                    renderItem={({ item }) => (
                        <Text style={styles.row}> {[item.id, item.title]} </Text>
                    )}
                    style={{ marginTop: 10 }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        paddingTop: 50,
        justifyContent: 'center',
        margin: 5,
        height: 800,
    },
    row: {
        fontSize: 18,
        padding: 12,
    },
    textInput: {
        textAlign: 'center',
        height: 42,
        borderWidth: 1,
        borderColor: '#009688',
        borderRadius: 8,
    },
})

export default API;