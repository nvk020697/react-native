import React, { Component } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, View, StatusBar, Button, TouchableOpacity, ScrollView } from 'react-native';

const DATA = [
    {
        id: '1',
        title: 'Kayle',
    },
    {
        id: '2',
        title: 'Varus',
    },
    {
        id: '3',
        title: 'Lulu',
    },
    {
        id: '4',
        title: 'Vayne',
    },
    {
        id: '5',
        title: 'Jana',
    },
    // {
    //     id: '6',
    //     title: 'Nami',
    // },
    // {
    //     id: '7',
    //     title: 'Ashe',
    // },
    // {
    //     id: '8',
    //     title: 'Oriana',
    // },
    // {
    //     id: '9',
    //     title: 'Ahri',
    // },
];

const Item = ({ title }) => (
    <TouchableOpacity
        onPress={() => {
            console.log('title', title)
        }}
        style={styles.item}>
        <Text style={styles.title}>
            {title}
        </Text>
    </TouchableOpacity>
);

const FlatListBasics = () => {
    const renderItem = ({ item }) => (
        <Item title={item.title} />
    )

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    item: {
        backgroundColor: '#5488c7',
        padding: 20,
        marginVertical: 1,
        alignItems: 'center'
    },
    title: {
        fontSize: 32,
        color: '#fff',
    }
});

export default FlatListBasics;
