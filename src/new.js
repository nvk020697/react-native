import React from 'react';
import { View, Text, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    center: {
        alignItems: 'center'
    }
})
const Greeting = (props) => {
    return (
        <View style={styles.center}>
            <Text>Hello {props.name}!!!</Text>
        </View>
    );
}
const LotsOfGreetings = () => {
    return (
        <View style={[styles.center, { top: 50 }]}>
            <Greeting name='Vayne' />
            <Greeting name='Lulu' />
            <Greeting name='Sona' />
        </View>
    );
}
export default LotsOfGreetings;