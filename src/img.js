import React from 'react';
import {
    View, Image, StyleSheet, TextInput, ScrollView, Text, Button, SafeAreaView,
    SectionList, StatusBar, FlatList
} from 'react-native'

const DATA = [
    {
        title: "Main dishes",
        data: ["Pizza", "Burger", "Risotto"]
    },
    {
        title: "Sides",
        data: ["French Fries", "Onion Rings", "Fried Shrimps"]
    },
    {
        title: "Drinks",
        data: ["Water", "Coke", "Beer"]
    },
    {
        title: "Desserts",
        data: ["Cheese Cake", "Ice Cream"]
    }
];

const drink = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];

const Item = ({ title }) => (
    <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
    </View>
);

const img = () => {
    const [value, onChangeText] = React.useState('Nhập từ khóa');
    const renderItem = ({ item }) => (
        <Item title={item.title} />
    );

    return (
        <View style={styles.container}>
            <ScrollView>
                <Image
                    style={styles.stretch}
                    source={require('../public/img/img.jpg')}
                />
                <View style={styles.input}>
                    <TextInput
                        onChangeText={text => onChangeText(text)}
                        value={value}
                        style={styles.inputContent}
                    ></TextInput>
                    <Image style={styles.imgChild}
                        source={require('../public/img/image_11.png')}
                    ></Image>
                </View>
                <View>
                    <View style={styles.div}>
                        <Text style={styles.text}>Hello</Text>
                        <Text style={styles.text}>I am Khánh</Text>
                        <Text style={styles.text}>I Love You</Text>
                    </View>
                </View>
                <View>
                    <Button
                        title="Press me"
                        style={styles.Button}
                    />
                </View>
                <SafeAreaView style={styles.container}>
                    <SectionList
                        sections={DATA}
                        keyExtractor={(item, index) => item + index}
                        renderItem={({ item }) => <Item title={item} />}
                        renderSectionHeader={({ section: { title } }) => (
                            <Text style={styles.header}>{title}</Text>
                        )}
                    />
                </SafeAreaView>
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={drink}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 0,
        marginHorizontal: 5,
        flex: 1
    },
    stretch: {
        width: '100%',
        height: 200,
        resizeMode: 'stretch',
    },
    input: {
        position: 'relative',
        padding: 5,
        backgroundColor: 'orange'
    },
    inputContent: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        paddingLeft: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
    },
    imgChild: {
        position: 'absolute',
        right: 15,
        top: 15,
    },
    div: {
        backgroundColor: 'green',
        padding: 5,
    },
    text: {
        fontSize: 14,
        color: '#fff'
    },
    Button: {
        width: 100,
        textAlign: 'center',
        alignItems: 'center',
    },
    item: {
        backgroundColor: "#f9c2ff",
        padding: 20,
        marginVertical: 10
    },
    header: {
        fontSize: 32,
        backgroundColor: "#fff"
    },
    title: {
        fontSize: 24
    }
});


export default img;