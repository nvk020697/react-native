import React, { useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from "react-native";

const ModalComponent = () => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
        <View style={styles.allView}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                presentationStyle={"overFullScreen"}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centerView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Hello World!</Text>

                        <TouchableHighlight
                            style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Text style={styles.textStyle}>Hide Modal</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>

            <TouchableHighlight
                style={styles.openButton}
                onPress={() => {
                    setModalVisible(true);
                }}
            >
                <Text style={styles.textStyle}>Show Modal</Text>
            </TouchableHighlight>
        </View>
    );
};

const styles = StyleSheet.create({
    allView: {
        flex: 1,
        alignItems: "center",
        paddingVertical: 20,
        paddingHorizontal: 10,

    },
    modalView: {
        margin: 20,
        backgroundColor: "pink",
        borderRadius: 10,
        padding: 35,
        alignItems: "center",
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 5,
        width: '100%',
    },
    centerView: {
        flex: 1,
        alignItems: "center",
        paddingHorizontal: 10,
        justifyContent: 'center',
        backgroundColor: '#00000066'
    },
    openButton: {
        backgroundColor: "#2196F3",
        borderRadius: 5,
        padding: 10,
        width: '100%',
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 45,
        textAlign: "center",
        color: '#fff',
        fontSize: 28,
        fontWeight: 'bold'
    }
});

export default ModalComponent;