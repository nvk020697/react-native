import React, { useState, useEffect } from 'react';
import {
    FlatList,
    Text,
    StyleSheet,
    View,
    TextInput,
} from 'react-native';

export default function ABCDEE() {
    const [text, setText] = useState('');
    const [dataArr, setDataArr] = useState([]);
    const [dataDisplay, setDataDisplay] = useState([]);

    const fetchAPI = () => {
        return fetch('https://api.covid19api.com/countries')
            .then(response => response.json())
            .then(data => {
                setDataArr(data);
                setDataDisplay(data);
                // console.log(data)
            })
            .catch(error => {
                console.error(error);
            });
    };

    useEffect(() => {
        fetchAPI();
    }, []); // use `[]` to avoid multiple side effect

    const itemSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: '100%',
                    backgroundColor: '#000',
                }}
            />
        );
    };

    const searchKey = (text) => {
        if (text === 0) {
            setDataDisplay(dataArr)
        }
        else {
            let result = dataArr.filter(item => item.Country.toUpperCase().indexOf(text.toUpperCase()) !== -1);
            // console.log('result', result);
            setDataDisplay(result);
        }
    }

    useEffect(() => {
        searchKey(text)
    }, [text]);

    return (
        <View>
            <View style={styles.MainContainer}>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => setText(text)}
                    value={text}
                    underlineColorAndroid="transparent"
                    placeholder="Search Here"
                />
                <FlatList
                    data={dataDisplay}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={itemSeparator}
                    renderItem={({ item }) => (
                        <Text style={styles.row}>{item.Country}</Text>
                    )}
                    style={{ marginTop: 10 }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        paddingTop: 50,
        justifyContent: 'center',
        margin: 5,
        height: 800,
    },

    row: {
        fontSize: 18,
        padding: 12,
    },

    textInput: {
        textAlign: 'center',
        height: 42,
        borderWidth: 1,
        borderColor: '#009688',
        borderRadius: 8,
    },
});