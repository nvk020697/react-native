import React from 'react';
import { View, TextInput, Text, StyleSheet } from 'react-native';

const SearchBar240 = (props) => {
    return (
        <View>
            <TextInput style={styles.textinput} />
            <View style={styles.div}>
                <Text style={styles.textP}>Thứ 3 Ngày 10-11-2020</Text>
                <Text style={styles.textP}>Date: 11h21'</Text>
                <Text style={styles.textP}>Nguyễn Văn Khánh</Text>
            </View>
            <View style={styles.div}>
                <Text style={styles.textP}>Thứ 3 Ngày 10-11-2020</Text>
                <Text style={styles.textP}>Date: 11h21'</Text>
                <Text style={styles.textP}>Nguyễn Quốc Bảo</Text>
            </View>
            <View style={styles.div}>
                <Text style={styles.textP}>Thứ 3 Ngày 10-11-2020</Text>
                <Text style={styles.textP}>Date: 11h21'</Text>
                <Text style={styles.textP}>Nguyễn Hương Ly</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    textinput: {
        borderWidth: 1,
        borderColor: "#000",
        margin: 10,
        paddingHorizontal: 10
    },
    div: {
        backgroundColor: 'green',
        padding: 10,
        borderBottomColor: '#fff',
        borderWidth: 1
    },
    textP: {
        color: '#fff'
    }
})

export default SearchBar240;