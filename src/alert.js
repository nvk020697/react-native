import React, { useRef, useState } from "react";
import { View, StyleSheet, Button, Alert, Text, TextInput } from "react-native";

const alertX = () => {
    const [alertT, setAlertT] = useState('')
    let textA = useRef(null);


    const handleChangeText = (text) => {
        textA.current = text
    }

    const createTwoButtonAlert = () =>
        Alert.alert(
            'Hello',
            //body
            'I am three option alert. Do you want to cancel me ?',
            [
                {
                    text: 'Save', onPress: () => {
                        console.log('save');
                        setAlertT(textA.current);
                        console.log('click handle change');
                    }
                },
                { text: 'Cacel', onPress: () => console.log('Cacel Pressed') },
                { text: 'Delete', onPress: () => console.log('Delete Pressed') },
            ],
            { cancelable: true }
        );

    return (
        <View style={styles.container}>
            <TextInput style={styles.input} onChange={handleChangeText} />
            <TextInput style={styles.input} onChange={handleChangeText} />
            <Button title={"Button Alert"} onPress={createTwoButtonAlert} />
            <Text>
                {alertT}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        marginHorizontal: 10
    },
    input: {
        width: '100%',
        borderWidth: 2,
        borderColor: "#ccc",
        margin: 10,
        paddingHorizontal: 10
    }
});

export default alertX;