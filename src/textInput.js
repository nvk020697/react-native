import React, { Component } from 'react';
import { TextInput } from 'react-native';

const UselessTextInput = () => {
    const [value, onChangeText] = React.useState('Useless Placeholder');

    handeChangeText = (text) => {
        console.log(text);
        onChangeText(text)
    }

    return (
        <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={handeChangeText}
            value={value}
        />
    );
}

export default UselessTextInput;
