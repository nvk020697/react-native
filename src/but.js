import React, { useState, useRef } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    view_container: {
        alignSelf: 'center',
        marginTop: 60,
        width: 300,
        height: height * 0.7,
        paddingTop: 30,
        paddingHorizontal: 25,
        backgroundColor: '#F59062',
    },
    view_info: {
        marginBottom: 20,
    },
    view_text: {
        minHeight: 35,
        marginBottom: 12,
        justifyContent: 'center',
        paddingHorizontal: 10,
        backgroundColor: 'white',
    },
    text: {
        fontSize: 14,
    },
    view_bottom: {
        flexDirection: 'row',
    },
    textbox: {
        minHeight: 35,
        width: 125,
        marginRight: 20,
        paddingHorizontal: 10,
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 3,
        fontSize: 14,
        backgroundColor: 'white',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        backgroundColor: 'orange',
    },
    text_button: {
        fontSize: 16,
        fontWeight: '600',
        color: 'white',
    },
});

const FormInfo = (props) => {
    const { defaultValue = '', styleButton, propsText, propsTextbox } = props;
    const [valueText, setValueText] = useState(defaultValue);
    const valueTextbox = useRef(defaultValue);

    handleChangeTextbox = (value) => {
        valueTextbox.current = value;
    };

    handleShowText = () => {
        setValueText(valueTextbox.current);
    };

    return (
        <View style={styles.view_info}>
            <View style={styles.view_text}>
                <Text style={styles.text} numberOfLines={1} {...propsText}>
                    {valueText}
                </Text>
            </View>

            <View style={styles.view_bottom}>
                <TextInput
                    style={styles.textbox}
                    onChangeText={handleChangeTextbox}
                    {...propsTextbox}
                />
                <TouchableOpacity
                    style={[styles.button, styleButton]}
                    onPress={handleShowText}>
                    <Text style={styles.text_button}>Show</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const ViewContainer = () => {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.view_container}>
                <FormInfo />
                <FormInfo
                    styleButton={{ backgroundColor: 'blue' }}
                    propsTextbox={{ multiline: true }}
                />
                <FormInfo
                    styleButton={{ backgroundColor: 'purple' }}
                    propsText={{ numberOfLines: 2 }}
                    propsTextbox={{ multiline: true }}
                />
            </View>
        </SafeAreaView>
    );
};

export default ViewContainer;
