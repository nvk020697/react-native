/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import AlertX from './src/alert';
import count from './src/count';
import FlatListBasics from './src/flastList';
import hello from './src/hello'
import img from './src/img';
import Js from './src/js';
import layout1 from './src/layout1';
import layout2 from './src/layout2';
import layout21 from './src/layout21';
import LotsOfGreetings from './src/new'
import SectionListBasics from './src/SectionList';
import showInput from './src/showInput';
import UselessTextInput from './src/textInput';
import ViewX from './src/View';
import alertX from './src/alert';
import MyFunction from './src/useEffect';
import ScrollBasic from './src/scroll';
import flatScroll from './src/flatScroll';
import AlertDemo from './src/AlertDemo';
import ModalComponent from './src/Modal';
import ReactNavigation from './src/navigation';
import StackNavigation from './src/StackNavigation';
import AddDataNavi from './src/AddDataNavi';
import bottomNavigation from './src/bottomNavigation';
import HeaderButton from './src/HeaderButton';
import Naviga220 from './src/Naviga220'
import NavigaDrawer from './src/NavigaDrawer'
import DrawerNavigaBot from './src/NavigaDrawerBottom';
import SearchBar240 from './src/SearchBar';
import FlatListDemo from './src/demo';
import DemoSearch from './src/demoSearch';
import ToolbarView from './src/demo'
import ABCDEE from './src/demo';
import API from './src/API';
import MyWeb from './src/webView';
// import ViewPagerPage from './src/ViewPage';

AppRegistry.registerComponent(appName, () => API);
