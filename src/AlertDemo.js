import React, { useState, useRef } from "react";
import { View, StyleSheet, Button, Alert, TextInput, Text } from "react-native";
import { inOut } from "react-native/Libraries/Animated/src/Easing";

const ItemTextInput = () => {
    const value = useRef('');
    const textInput = useRef(null)

    handleChangeText = (text) => {
        value.current = text;
    }

    refTextInput = (ref) => {
        textInput.current = ref
    }

    clearTextInput = () => {
        textInput.current.clear()
    }

    return {
        getValue: value.current,
        setRef: refTextInput,
        setValue: handleChangeText,
        clearValue: clearTextInput
    }
}

const AlertDemo = () => {

    const [textA, setTextA] = useState('');
    const [textB, setTextB] = useState('')

    const textInputA = useRef('')
    const refTextA = useRef('')
    const textInputB = useRef('')
    const refTextB = useRef('')

    handleChangeText = (title, text) => {
        console.log(text, title)
        title.current = text
    }

    refTextInput = (title, ref) => {
        title.current = ref
    }

    createThreeButtonAlert = () => {
        Alert.alert(
            "Alert Title",
            "My Alert Msg",
            [
                {
                    text: "Save",
                    onPress: () => {
                        console.log("Save", textInputA.current, textInputB.current);
                        setTextA(textInputA.current)
                        setTextB(textInputB.current)
                    }
                },
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel"),
                    style: "cancel"
                },
                {
                    text: "Delete", onPress: () => {
                        console.log("Delete");
                        setTextA('')
                        setTextB('')
                        refTextA.current.clear()
                        refTextB.current.clear()
                    }
                }
            ],
            { cancelable: true }
        );
    }


    renderTextInput = (title) => {
        return (
            <TextInput
                style={styles.input}
                onChangeText={(text) => handleChangeText(`textInput${title}`, text)}
                ref={(ref) => refTextInput(`refText${title}`, ref)} />
        )
    }

    return (
        <View style={styles.container}>
            {renderTextInput('A')}
            {renderTextInput('B')}
            <Button title={"Button Alert"} onPress={createThreeButtonAlert} />
            <Text>
                {textA}
            </Text>
            <Text>
                {textB}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        marginHorizontal: 10
    },
    input: {
        width: '100%',
        borderWidth: 2,
        borderColor: "#ccc",
        margin: 10,
        paddingHorizontal: 10
    }
});

export default AlertDemo;