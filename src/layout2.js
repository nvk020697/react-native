import React, { useState } from 'react';
import {
    View, StyleSheet, Dimensions, Text, TextInput,
    TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';

const { width, height } = Dimensions.get('window');

const layout2 = () => {
    const [isShow, setIsShow] = useState('');
    const [isText, setIsText] = useState('');

    handleClickButton = () => {
        setIsShow(isText)
    }

    handleClickDele = () => {
        setIsShow('');
        setIsText('')
    }

    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <View style={styles.textOne}>
                    <Text style={styles.disable} editable={false} multiline={false} numberOfLines={2}>
                        {isShow}
                    </Text>
                    <View style={styles.content}>
                        <TextInput
                            style={styles.contentIn}
                            onChangeText={(text) => setIsText(text)}
                            value={isText}
                        />
                        <TouchableHighlight onPress={handleClickButton}>
                            <View style={styles.button} >
                                <Text style={styles.btntext}>Show</Text>
                            </View>
                        </TouchableHighlight>
                        <TouchableOpacity onPress={handleClickDele}>
                            <View style={styles.button} >
                                <Text style={styles.btntext}>Dele</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.textOne}>
                    <Text style={styles.disable} editable={false} />
                    <View style={styles.content}>
                        <TextInput style={styles.contentIn} multiline={true}
                            scrollEnabled></TextInput>
                        <TouchableOpacity>
                            <View style={styles.button} >
                                <Text style={styles.btntext}>Show</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.textOne}>
                    <Text style={styles.disable} editable={false} numberOfLines={2} />
                    <View style={styles.content}>
                        <TextInput style={styles.contentIn} multiline={true}></TextInput>
                        <TouchableWithoutFeedback>
                            <View style={styles.button} >
                                <Text style={styles.btntext}>Show</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>

            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        alignItems: "center"
    },
    body: {
        width: 300,
        height: height * (7 / 10),
        backgroundColor: "green",
        alignItems: "center",
        paddingTop: 30
    },
    textOne: {
        marginBottom: 20,
    },
    disable: {
        minHeight: 40,
        width: 250,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: "#fff",
        marginBottom: 12
    },
    content: {
        flexDirection: "row",
    },
    contentIn: {
        backgroundColor: '#fff',
        marginRight: 20,
        borderRadius: 5,
        paddingHorizontal: 10,
        flex: 1,
        textAlignVertical: "top",
    },
    button: {
        alignItems: "center",
        backgroundColor: "#fe4123",
        padding: 10,
        borderRadius: 5,
    },
    btntext: {
        color: "#fff",
        fontSize: 16,
    }
})

export default layout2;