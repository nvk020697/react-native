import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const getRandomColor = () => {
    const COLOR_LIST = ['deepink', 'green', 'yellow', 'black', 'blue'];
    const randomIndex = Math.trunc(Math.random() * 5);
    return COLOR_LIST[randomIndex];
}

const Js = () => {
    // executed before each render
    const [color, setColor] = useState('deeppink');
    const [text, setText] = useState('string')

    useEffect(() => {
        console.log('userEffect')
        return () => {
            console.log('update')
        }
    }, [color]);


    setColor('hgfghf')

    return (
        <TouchableOpacity>
            <View style={{ backgroundColor: color }}>
                <Text>Hello</Text>
            </View >
        </TouchableOpacity>
    )
}
export default Js;