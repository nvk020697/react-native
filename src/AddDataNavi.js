import * as React from 'react';
import { useState, useRef, useEffect } from 'react';
import { Button, View, StyleSheet, Text, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function HomeScreen({ navigation }) {
    const [textName, setTextName] = useState('');
    const [textPass, setTextPass] = useState('');

    handleClickDelete = () => {
        console.log('dele')
    }

    return (
        <View style={styles.container}>
            <Text style={styles.labeltext}>Home Screen</Text>

            <View style={styles.body}>
                <View style={[styles.content, styles.marginBT]}>
                    <Text style={styles.contentText}>Name: </Text>
                    <TextInput style={styles.contentInput} onChangeText={(text) => { setTextName(text) }} />
                </View>
                <View style={styles.content}>
                    <Text style={styles.contentText}>PassWord: </Text>
                    <TextInput
                        secureTextEntry={true}
                        style={styles.contentInput}
                        onChangeText={(text) => { setTextPass(text) }} />
                </View>
            </View>

            <Button
                title="Next Screen Update Password"
                onPress={() => navigation.navigate('Profile', { name: textName, pass: textPass })}
            />

            <Button
                title="Delete"
                onPress={handleClickDelete}
            />
        </View>
    );
}

function ProfileScreen(props) {
    useEffect(() => {
        console.log('params navigation', props.route.params);
    }, [])

    return (
        <View style={styles.container}>
            <Text style={styles.labeltext}>Profile Screen</Text>

            <View style={styles.body}>
                <Text style={styles.contentItem}>{props.route.params.name}</Text>
                <Text style={styles.contentItem}>{props.route.params.pass}</Text>
            </View>

            <Button title="Go back" onPress={() => props.navigation.goBack()} />
        </View>
    );
}

const Stack = createStackNavigator();

function StackNavigationA() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
        </Stack.Navigator>
    );
}

const AddDataNavi = () => {
    return (
        <NavigationContainer>
            <StackNavigationA />
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 30
    },
    labeltext: {
        padding: 10,
        backgroundColor: 'orange',
        color: '#fff',
        width: '100%',
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20
    },
    body: {
        padding: 10,
        backgroundColor: '#21a465',
        width: '100%',
        marginBottom: 20,
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    marginBT: {
        marginBottom: 15
    },
    contentText: {
        flex: 1,
        color: '#fff',
        fontSize: 18
    },
    contentInput: {
        borderWidth: 2,
        borderColor: '#fff',
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        flex: 2
    },
    contentItem: {
        color: '#fff',
        fontSize: 16
    }
})

export default AddDataNavi;