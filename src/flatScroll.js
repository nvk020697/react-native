import React, { useRef, useState } from 'react';
import { View, StyleSheet, Text, Button, ScrollView, TextInput, TouchableOpacity } from 'react-native';

const flatScroll = () => {
    const [textScroll, setTextScroll] = useState('');
    let textT = useRef(null);

    handleClickButton = () => {
        setTextScroll(textT.current)
    }

    handleChangeText = (text) => {
        textT.current = text
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={{ color: '#fff' }}>
                    {textScroll}
                </Text>
                <TextInput style={styles.input} onChangeText={handleChangeText} />
                <TouchableOpacity onPress={handleClickButton}>
                    <Text>
                        Button
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        padding: 10,
        backgroundColor: 'red',
    },
    input: {
        borderWidth: 2,
        borderColor: "#ccc",
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        height: 50,
        paddingHorizontal: 10,
        fontSize: 14
    }
})

export default flatScroll;