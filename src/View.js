import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const ViewX = () => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerTop}></View>
                <View style={styles.headerCont}>
                    <View style={[styles.hdChild, styles.hdChild1]} />
                    <View style={styles.hdChild} />
                </View>
            </View>

            <View style={styles.ContentChild}></View>
            <View style={styles.footer}>
                <View style={styles.footerChild}></View>

                <View style={styles.footerChild1a}></View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    header: {
        height: height / 6,
        marginTop: 80,
        marginBottom: 40,
        marginHorizontal: 20,
        padding: 15,
        borderColor: "#655e5e",
        borderWidth: 1,
        alignItems: "center",
        borderRadius: 5
    },
    headerTop: {
        backgroundColor: "#1da3ae",
        width: 200,
        height: 35,
    },
    headerCont: {
        flex: 1,
        marginTop: 15,
        flexDirection: "row",
    },
    hdChild: {
        backgroundColor: "#f59062",
        borderRadius: 5,
        flex: 1,
    },
    hdChild1: {
        marginRight: 20
    },
    ContentChild: {
        backgroundColor: "#948a8a",
        width: 300,
        height: 90,
        marginBottom: 50,
        alignSelf: 'center'
    },
    footer: {
        marginLeft: 30,
        marginBottom: 45,
        flexDirection: "row",
        flex: 1,
    },
    footerChild: {
        borderColor: "#655e5e",
        borderWidth: 1,
        width: 100,
        marginRight: 20,
        borderRadius: 10,
    },
    footerChild1a: {
        width: 100,
        backgroundColor: "#cf5353",
        height: 100,
        borderRadius: 10,
        alignSelf: 'center'
    }

})

export default ViewX;