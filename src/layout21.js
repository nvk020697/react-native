import React, { useState, useRef } from 'react';
import {
    View, StyleSheet, Dimensions, Text, TextInput,
    TouchableOpacity
} from 'react-native';
const { width, height } = Dimensions.get('window');

const FormIF = (props) => {
    const { propsStyle, propsText } = props;
    const [textC, setTextC] = useState('');
    let textF = useRef(null);

    handleClickButtonC = () => {
        setTextC(textF.current)
    }

    handleChangTextC = (text) => {
        console.log(text);
        textF.current = text
    }

    return (
        <View style={styles.textOne}>
            <Text style={styles.disable} editable={false}
                {...propsText}
            >
                {textC}
            </Text>
            <View style={styles.content}>
                <TextInput
                    style={styles.contentIn}
                    onChangeText={handleChangTextC}
                    {...propsStyle}
                />
                <TouchableOpacity onPress={handleClickButtonC}>
                    <View style={styles.button}>
                        <Text style={styles.btntext}>Show</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const layout21 = () => {
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <FormIF propsText={{ numberOfLines: 1 }} />
                <FormIF
                    propsStyle={{ multiline: true }}
                    propsText={{ numberOfLines: 1 }}
                />
                <FormIF
                    propsStyle={{ multiline: true }}
                    propsText={{ numberOfLines: 2 }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        alignItems: "center"
    },
    body: {
        width: 300,
        height: height * (7 / 10),
        backgroundColor: "green",
        alignItems: "center",
        paddingTop: 30
    },
    textOne: {
        marginBottom: 20,
    },
    disable: {
        minHeight: 40,
        width: 250,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: "#fff",
        marginBottom: 12,
        padding: 5
    },
    content: {
        flexDirection: "row",
    },
    contentIn: {
        backgroundColor: '#fff',
        marginRight: 20,
        borderRadius: 5,
        paddingHorizontal: 10,
        flex: 1,
        textAlignVertical: "top",
    },
    button: {
        alignItems: "center",
        backgroundColor: "#fe4123",
        padding: 10,
        borderRadius: 5,
    },
    btntext: {
        color: "#fff",
        fontSize: 16,
    }
})

export default layout21;