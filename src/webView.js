import React, { useState } from 'react';
import { WebView } from 'react-native-webview';
import { View, StyleSheet, SafeAreaView, ActivityIndicator } from 'react-native'

const ActivityIndicatorElement = () => {
    return (
        <View style={styles.activityIndicatorStyle}>
            <ActivityIndicator color="#000" size="large" />
        </View>
    );
};

const MyWeb = () => {
    const [visible, setVisible] = useState(false);
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.container}>
                <WebView
                    style={{ flex: 1 }}
                    //Loading URL
                    source={{ uri: 'https://aoe-vn.netlify.app/' }}
                    //Enable Javascript support
                    javaScriptEnabled={true}
                    //For the Cache
                    domStorageEnabled={true}
                    onLoadStart={() => setVisible(true)}
                    onLoad={() => setVisible(false)}
                />
                {visible ? <ActivityIndicatorElement /> : null}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
    },
    activityIndicatorStyle: {
        flex: 1,
        position: 'absolute',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 'auto',
        marginBottom: 'auto',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
})

export default MyWeb;