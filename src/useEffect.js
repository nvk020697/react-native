import React, { useState, useEffect, useRef } from "react";                      // first, we import useRef from react
import { View, TextInput, Text } from "react-native";

const MyFunction = () => {
    const [data, setData] = useState([]);
    const [enteredFilter, setEnteredFilter] = useState("");
    const inputRef = useRef();                                                     // second, we initialize a variable by useRef

    const fetchData = async () => {
        let response = await fetch("https://jsonplaceholder.typicode.com/users/");
        let responseData = await response.json();
        let filteredData = responseData.filter(item => {
            return item.name.includes(enteredFilter);
        });
        setData(filteredData);
    };

    useEffect(() => {
        const timer = setTimeout(() => {                                             // third, we create a setTimeout function to wrap
            if (enteredFilter === inputRef.current.value) {                            // fourth,  an if block to check and validate
                fetchData();
            }
        }, 500);                                                                     // fifth, desired timeout amount
    }, [enteredFilter, inputRef]);

    return (
        <View>
            <TextInput
                ref={inputRef}                                                           // sixth, assigning the special ref prop to previously initialized inputRef
                placeholder="Search Names"
                value={enteredFilter}
                onChange={e => setEnteredFilter(e.target.value)}
            />
            <View>
                {data.map(el => (
                    <View key={el.id}>
                        <Text>{el.name}</Text>
                    </View>
                ))}
            </View>
        </View>
    );
}

export default MyFunction;