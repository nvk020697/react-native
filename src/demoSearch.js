import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TextInput, StatusBar } from 'react-native';

const DATA = [
    {
        id: 'HUBT-1',
        date: '2011',
        time: '10h10',
        name: 'Nguyen Van A'
    },
    {
        id: 'HUBT-2',
        date: '2012',
        time: '11h11',
        name: 'Dinh Van B'
    },
    {
        id: 'HUBT-3',
        date: '2013',
        time: '12h12',
        name: 'Le Van C'
    },
    {
        id: 'HUBT-4',
        date: '2014',
        time: '13h13',
        name: 'Le Huong Giang'
    },
    {
        id: 'HUBT-5',
        date: '2014',
        time: '13h13',
        name: 'Le Huong Giang'
    },
];
const DemoSearch = () => {
    const [search, setSearch] = useState('');

    const Item = ({ data }) => (
        <View style={styles.item}>
            <Text style={styles.title}>{data.id}</Text>
            <Text style={styles.title}>{data.date}</Text>
            <Text style={styles.title}>{data.time}</Text>
            <Text style={styles.title}>{data.name}</Text>
        </View>
    );
    const renderItem = ({ item }) => (
        <Item data={item} />
    );
    return (
        <View>
            <TextInput
                style={styles.textinput}
                placeholder='Search !!!'
                value={search}
                onChangeText={(text) => setSearch(text)}
            />
            <FlatList
                data={DATA}
                keyExtractor={(item, index) => item.id}
                renderItem={renderItem}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 18,
    },
    textinput: {
        margin: 10,
        borderWidth: 1,
        paddingHorizontal: 10,
        borderColor: '#333'
    }
});

export default DemoSearch;